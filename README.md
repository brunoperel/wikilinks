WikiLinks
=========

Graph (d3) -based representation of relationships between Wikipedia articles.


The graph use a force-directed layout.

And here's a [demo](http://87.106.165.63/WikiLinks/) !
